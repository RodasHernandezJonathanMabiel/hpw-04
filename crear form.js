/*CREAR FORM*/
function crear_form( atributos )
{
    var obj_temporal = document.createElement( "form" );
    
    for( var i = 0; i < atributos.length; i++ )
        obj_temporal.setAttribute( atributos[i]["name"], atributos[i]["value"] );
    return obj_temporal;
}

/*CREAR INPUT*/
function crear_input( atributos )
{
    var obj_temporal = document.createElement( "input" );
    
    for( var i = 0; i < atributos.length; i++ )
    {
        if( atributos[i]["name"] === "textContent" )
            obj_temporal.textContent = atributos[i]["value"]
        obj_temporal.setAttribute( atributos[i]["name"], atributos[i]["value"] );
    }
    return obj_temporal;
}
asoriar_hijo( textarea, form );
/*CREAR SELECT*/
function crear_select( atributos, opciones )
{
    var obj_temporal = document.createElement( "select" );
    
    for( var i = 0; i < atributos.length; i++ )
        obj_temporal.setAttribute( atributos[i]["name"], atributos[i]["value"] );
    for( i = 0; i < opciones.length; i++ )
    {
        var obj_opcion;
        obj_opcion = crear_option( opciones[i] );
        obj_temporal.appendChild( obj_opcion );
    }
    return obj_temporal;
}
/*COMPLEMENTO PARA CREAR SELECT (CREAR OPTION)*/
function crear_option( atributos )
{
    var obj_temporal = document.createElement("option");
    for( var i = 0; i < atributos.length; i++ )
    {
        if( atributos[i]["name"] === "textContent" )
            obj_temporal.textContent = atributos[i]["value"]
        obj_temporal.setAttribute( atributos[i]["name"], atributos[i]["value"] );
    }
    return obj_temporal;
}

/*CREAR TEXTAREA*/
function crear_textarea( atributos )
{
    var obj_temporal = document.createElement( "textarea" );
    
    for( var i = 0; i < atributos.length; i++ )
    {
        if( atributos[i]["name"] === "textContent" )
            obj_temporal.textContent = atributos[i]["value"]
        obj_temporal.setAttribute( atributos[i]["name"], atributos[i]["value"] );
    }
    return obj_temporal;
}

/*ASOCIAR PADRE/HIJO*/
function asociar_hijo( hijo, padre )
{
    padre.appendChild( hijo );
}


form = crear_form( atributos_form );
input_text = crear_input( atributos_input_text );
input_radio = crear_input( atributos_input_radio );
input_radio2 = crear_input( atributos_input_radio2 );
input_email = crear_input( atributos_input_email );

textarea = crear_textarea( atributos_input_textarea );

select = crear_select( atributos_select, atributos_option );

asociar_hijo( input_text, form );
asociar_hijo( input_radio, form );
asociar_hijo( input_radio2, form );
asociar_hijo( input_email, form );
asociar_hijo( select, form );
asociar_hijo( textarea, form );
asociar_hijo( form, document.getElementsByTagName("body")[0] );


atributos_form = 
    [
        { "name" : "id", "value" : "form"},
        { "name" : "name", "value" : "form" },
        { "name" : "method", "value" : "POST" }
    ];

atributos_select = 
    [
        { "name" : "id", "value" : "select" }
    ];

atributos_option =
    [
        [
            { "name" : "id", "value" : "op1" },
            { "name" : "value", "value" : "1" },
            { "name" : "textContent", "value" : "Opcion 1" }
        ],
        [
            { "name" : "id", "value" : "op2" },
            { "name" : "value", "value" : "2" },
            { "name" : "textContent", "value" : "Opcion 2" }
        ],
        [
            { "name" : "id", "value" : "op3" },
            { "name" : "value", "value" : "3" },
            { "name" : "textContent", "value" : "Opcion 3" }
        ]
    ];

atributos_input_text = 
    [ 
        {"name" : "type", "value" : "text"},
        {"name" : "id", "value" : "texto"},
        {"name" : "name", "value" : "input_text"}
    ];
    
atributos_input_textarea = 
    [ 
        {"name" : "type", "value" : "textarea"},
        {"name" : "id", "value" : "textoarea"},
        {"name" : "name", "value" : "input_text"}
    ];


atributos_input_radio = 
    [ 
        {"name" : "type", "value" : "radio"},
        {"name" : "id", "value" : "radio"},
        {"name" : "name", "value" : "input_radio"}
    ];
    
atributos_input_radio2 = 
    [ 
        {"name" : "type", "value" : "radio"},
        {"name" : "id", "value" : "radio2"},
        {"name" : "name", "value" : "input_radio"},
    ];
    
atributos_input_email = 
    [ 
        {"name" : "type", "value" : "email"},
        {"name" : "id", "value" : "email"},
        {"name" : "name", "value" : "input_email"},
        {"name" : "placeholder", "value" : "e.j.@algo.com" } 
    ];
    